# ReviewTanuki

A tool for handling Trainee Maintainer issues.

Heavily inspired by [merge-requests-tracker](https://gitlab.com/mayra-cabrera/merge-requests-tracker) and [pto-tanuki](https://gitlab.com/yorickpeterse/pto-tanuki)

## Usage

Get yourself an API private token with the "api" scope. You can now run
Review Tanuki using one of the following methods:

1. Locally (e.g. on a Raspberry Pi or a laptop)
1. On a server
2. On GitLab CI, using CI schedules

In all cases, it comes down to running the following command:

    env REVIEW_TANUKI_ISSUE='...' GITLAB_API_PRIVATE_TOKEN='...' GITLAB_API_ENDPOINT='https://gitlab.com/api/v4' bundle exec ./bin/review-tanuki

All environments variables are required. 

* The `REVIEW_TANUKI_ISSUE` variable is your trainee issue number on [`gitlab-com/www-gitlab-com`](https://gitlab.com/gitlab-com/www-gitlab-com/issues?label_name%5B%5D=trainee+maintainer).
* The `GITLAB_API_ENDPOINT` variable is already set if you run this in GitLab CI.

There's an optional variable, `REVIEW_TANUKI_DRY_RUN`, when set review tanuki will not edit your issue.

How you run this command periodically when running it locally or on a server is
up to you. Most likely you will want to set up a cronjob of some sort.

When using GitLab CI, you will have to fork the repository, then set up a CI
schedule with the following settings:

* Description: whatever you prefer (e.g. "Trainee Backend Maintainer")
* Interval pattern: [`0 5 * * *`](https://crontab.guru/#0_5_*_*_*). This
  will run the job once a day at 5AM. Other cron compatible patterns are also fine.
* Cron Timezone: your timezone
* Target branch: `master`
* Variables:
  * `GITLAB_API_PRIVATE_TOKEN`: your API token obtained earlier
  * `REVIEW_TANUKI_ISSUE`: your issue number obtained earlier
* Activated: Active


## License

ReviewTanuki is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
